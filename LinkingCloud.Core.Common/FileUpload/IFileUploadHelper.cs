﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkingCloud.API.Common.FileUpload
{
    public interface IFileUploadHelper
    {
        bool ConnectState(string path, string userName, string passWord);
    }
}
