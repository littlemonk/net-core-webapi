﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace LinkingCloud.API.Model
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("Interface_CardBind")]
    public partial class Interface_CardBind
    {
           public Interface_CardBind(){


           }
           /// <summary>
           /// Desc:医院用户唯一标识
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public string HospitalUserID {get;set;}

           /// <summary>
           /// Desc:卡号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardNo {get;set;}

           /// <summary>
           /// Desc:外部接入系统ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenUserID {get;set;}

           /// <summary>
           /// Desc:实名制身份证号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenIDCard {get;set;}

           /// <summary>
           /// Desc:实名制用户姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenUserName {get;set;}

           /// <summary>
           /// Desc:实名制手机号码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenTel {get;set;}

           /// <summary>
           /// Desc:HIS患者ID
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PatientID {get;set;}

           /// <summary>
           /// Desc:患者姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PatientName {get;set;}

           /// <summary>
           /// Desc:性别
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Sex {get;set;}

           /// <summary>
           /// Desc:生日
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Birth {get;set;}

           /// <summary>
           /// Desc:年龄
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Age {get;set;}

           /// <summary>
           /// Desc:患者身份证
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string IDCard {get;set;}

           /// <summary>
           /// Desc:患者手机号码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Tel {get;set;}

           /// <summary>
           /// Desc:卡类型：0 自费卡  1 医保卡  2社保卡
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardType {get;set;}

           /// <summary>
           /// Desc:卡属性 [0]本人 [1]父母 [2]子女 [3]配偶 [4]朋友
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardProperty {get;set;}

           /// <summary>
           /// Desc:1有效 0无效
           /// Default:
           /// Nullable:True
           /// </summary>           
           public bool? Status {get;set;}

    }
}
