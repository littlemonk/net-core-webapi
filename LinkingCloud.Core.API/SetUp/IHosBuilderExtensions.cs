﻿using Autofac.Extensions.DependencyInjection;

namespace LinkingCloud.Core.API.SetUp
{
    /// <summary>
    /// IHostBuilder 拓展
    /// </summary>
    public static class IHosBuilderExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostBuilder"></param>
        /// <returns></returns>
        public static IHostBuilder UseAutofac(this IHostBuilder hostBuilder) 
        {
            return hostBuilder.UseServiceProviderFactory(new AutofacServiceProviderFactory());
        }
    }
}
