﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkingCloud.API.Common.FTP
{
    public interface IFtpHelper
    {
        void FtpUpLoad(string filePath, string fileName, string folder);
    }
}
