﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkingCloud.API.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class FileUploadConst
    {
        /// <summary>
        /// 上传地址
        /// </summary>
        public string UploadPath { get; set; }

        /// <summary>
        /// 文件访问/下载地址
        /// </summary>
        public string DownloadPath { get; set; }

        /// <summary>
        /// 访问共享目录用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 访问共享目录密码
        /// </summary>
        public string Password { get; set; }
    }
}
