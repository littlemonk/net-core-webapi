﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkingCloud.API.Repository.suger
{
    public class BaseDBConfig
    {
        /// <summary>
        /// 测试只读数据库连接字符串
        /// </summary>
        public static string TestReadConnectionString { get; set; }
        /// <summary>
        /// 测试读写数据库连接字符串
        /// </summary>
        public static string TestWriteConnectionString { get; set; }
        /// <summary>
        /// 只读数据库连接字符串
        /// </summary>
        public static string ReadConnectionString { get; set; }
        /// <summary>
        /// 读写数据库连接字符串
        /// </summary>
        public static string WriteConnectionString { get; set; }

    }
}
