﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkingCloud.API.Common.RabbitMQ
{
    public interface IRabbitMQHelper
    {
        /// <summary>
        /// 发送消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queName"></param>
        /// <param name="msg"></param>
        void SendData<T>(string queName,string msg);

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queName"></param>
        /// <param name="msg"></param>
        void SendData<T>(string queName, List<string> msgList);
        /// <summary>
        /// 消费消息
        /// </summary>
        /// <param name="queName"></param>
        /// <param name="received"></param>
        void Receive(string queName, Action<string> received);
    }
}
