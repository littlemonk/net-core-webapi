﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinkingCloud.Core.API.Filter
{
    public class ActionAttribute : Attribute, IActionFilter
    {
        /// <summary>
        /// action执行之后
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuted(ActionExecutedContext context)
        {

        }
        /// <summary>
        /// action执行之前
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            string client_id = context.HttpContext.Request.Headers["client_id"];//用户名
            string client_secret = context.HttpContext.Request.Headers["client_secret"];//密码
            string controller = context.RouteData.Values["Controller"].ToString();//获取请求Controller名
            string action = context.RouteData.Values["Action"].ToString();//获取Action

        }
    }
}
