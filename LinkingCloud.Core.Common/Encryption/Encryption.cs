﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace LinkingCloud.API.Common
{
    public class Encryption: IEncryption
    {
        public string Md5(string value)
        {

            byte[] sor = Encoding.UTF8.GetBytes(value);
            MD5 md5 = MD5.Create();
            byte[] result = md5.ComputeHash(sor);
            StringBuilder strbul = new StringBuilder(40);
            for (int i = 0; i < result.Length; i++)
            {
                //加密结果"x2"结果为32位,"x3"结果为48位,"x4"结果为64位
                strbul.Append(result[i].ToString("x2"));
            }
            return strbul.ToString();
        }
    }
}
