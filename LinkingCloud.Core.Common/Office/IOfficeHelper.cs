﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace LinkingCloud.API.Common.Office
{
    public interface IOfficeHelper
    {
        DataTable ReadExcelToDataTable(string fileName, string sheetName = null, bool isFirstRowColumn = true);

        DataTable ReadStreamToDataTable(Stream fileStream, string sheetName = null, bool isFirstRowColumn = true);
    }
}
