﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace LinkingCloud.API.Model
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("Interface_Order")]
    public partial class Interface_Order
    {
           public Interface_Order(){


           }
           /// <summary>
           /// Desc:HIS 预约ID
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true)]
           public string OutTradeNo {get;set;}

           /// <summary>
           /// Desc:预约人姓名
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string PatientName {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OpenUserID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string HospitalUserID {get;set;}

           /// <summary>
           /// Desc:
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TradeType {get;set;}

           /// <summary>
           /// Desc:预约状态 0已取消 1已预约
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string OrderStatus {get;set;}

           /// <summary>
           /// Desc:卡号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string CardNo {get;set;}

           /// <summary>
           /// Desc:科室编号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DeptCode {get;set;}

           /// <summary>
           /// Desc:科室名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DeptName {get;set;}

           /// <summary>
           /// Desc:医生
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string DocName {get;set;}

           /// <summary>
           /// Desc:预约日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string BookDate {get;set;}

           /// <summary>
           /// Desc:开始时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TimeStart {get;set;}

           /// <summary>
           /// Desc:结束时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TimeEnd {get;set;}

           /// <summary>
           /// Desc:午别：上午  下午
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TimeDesc {get;set;}

           /// <summary>
           /// Desc:总金额
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string TotalAmount {get;set;}

           /// <summary>
           /// Desc:预约序号
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string QueuingNumber {get;set;}

           /// <summary>
           /// Desc:创建时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CreateTime {get;set;}

           /// <summary>
           /// Desc:取消时间
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? CancelTime {get;set;}

    }
}
