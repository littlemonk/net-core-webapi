﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkingCloud.API.Common
{
    public static class EntityConvert<T>
    {
        /// <summary>
        /// Entity 转 Dictionary
        /// </summary>
        /// <param name="model"></param>
        /// <param name="notColumn"></param>
        /// <returns></returns>
        public static Dictionary<String, object> EntityToDictionary(T model, string notColumn = "")
        {
            Dictionary<String, object> dic = new Dictionary<String, object>();
            foreach (var item in model.GetType().GetProperties())
            {
                if (!string.IsNullOrEmpty(notColumn) && notColumn.IndexOf(item.Name) != -1) continue;
                dic.Add(item.Name, item.GetValue(model, null));
            }
            return dic;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="notColumn"></param>
        /// <returns></returns>
        public static string EntityToHttpGetPamrate(T model, string notColumn = "")
        {
            string parmate = "";
            foreach (var item in model.GetType().GetProperties())
            {
                if (!string.IsNullOrEmpty(notColumn) && notColumn.IndexOf(item.Name) != -1) continue;
                parmate += "&" + item.Name + "=" + item.GetValue(model, null);
            }
            parmate = parmate.Substring(1);
            return parmate;
        }

    }
}
