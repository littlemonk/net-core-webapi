using Autofac;
using LinkingCloud.API.Common.Helper;
using LinkingCloud.Core.API.Filter;
using LinkingCloud.Core.API.SetUp;

var builder = WebApplication.CreateBuilder(args);
var Configuration = builder.Configuration;
//注入Log4Net
builder.Services.AddLogging(cfg =>
{
    //cfg.AddLog4Net();
    //默认的配置文件路径是在根目录，且文件名为log4net.config
    //如果文件路径或名称有变化，需要重新设置其路径或名称
    //比如在项目根目录下创建一个名为cfg的文件夹，将log4net.config文件移入其中，并改名为log.config
    //则需要使用下面的代码来进行配置
    cfg.AddLog4Net(new Log4NetProviderOptions()
    {
        Log4NetConfigFileName = "Config/Log4net.config",
        Watch = true
    });
});

// Add services to the container.
builder.Services.AddSingleton(new Appsettings(Configuration));
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

//注册Swagger
builder.Services.AddSwaggerSetup();
//builder.Services.AddSwaggerGen();
//jwt授权验证
builder.Services.AddAuthorizationSetup();
//使用Autofac
builder.Host.UseAutofac();
builder.Host.ConfigureContainer<ContainerBuilder>(ContainerBuilder =>
{
    ContainerBuilder.RegisterModule(new AutofacModuleRegister());
});

//全局异常
builder.Services.AddControllers(option =>
{
    option.Filters.Add(typeof(GlobalExceptionsFilter));
    //option.Filters.Add(typeof(ActionAttribute));//全局拦截
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    //启动Swagger
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

//app.MapControllers();

//终结点中间件，这里是配置，配置中间件和路由之间关系，映射
app.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}"
        );
app.Run();
