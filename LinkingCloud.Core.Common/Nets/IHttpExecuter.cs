﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LinkingCloud.API.Common.Nets
{
    public interface IHttpExecuter
    {
        Task<string> GetAsync(string url, Dictionary<string, string> headers = null);
        Task<string> PostAsync(string url, string parameters, Dictionary<string, string> headers = null);

        string Post(string url, string parameters, Dictionary<string, string> headers = null);

        Task<string> PostAsync(string url, string parameters, string date, Dictionary<string, string> headers = null);
        Task<string> PostByTextAsync(string url, string parameters, object URLParameters = null);
        Task<string> HttpPostAsync(string url, NameValueCollection data, string charset = "utf-8");
        string HttpPost(string url, NameValueCollection data, string charset = "utf-8");

        Task<string> PostFormFileAsync(string url, Dictionary<string, object> parameters, Dictionary<string, string> headers = null);

        Task<string> DeleteAsync(string url, string parameters, Dictionary<string, string> headers = null);
        Task<string> PutAsync(string url, string parameters, Dictionary<string, string> headers = null);

        Task<string> PatchAsync(string url, string parameters, Dictionary<string, string> headers = null);
        Task<MemoryStream> DownLoadFielToMemoryStream(string url);
        Task<string> SendAsync(string url, string parameters, string method, Dictionary<string, string> headers = null);
    }
}
