﻿namespace LinkingCloud.API.EasyPlatform
{
    public class BLLParameter
    {
        public string Name { get;  set; }
        public string ClassNamespace { get;  set; }
    }
}