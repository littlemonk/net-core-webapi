﻿using Microsoft.AspNetCore.Mvc;

namespace LinkingCloud.Core.API.Controllers
{
    /// <summary>
    /// 测试
    /// </summary>
    public class TestController : BaseController
    {
        private readonly ILogger<TestController> _logger;
        public TestController(ILogger<TestController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 测试接口
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public void TestLog()
        {
            _logger.LogInformation("我是测试Log");
        }
    }
}
