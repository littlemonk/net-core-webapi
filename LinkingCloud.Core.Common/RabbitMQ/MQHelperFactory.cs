﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkingCloud.API.Common.RabbitMQ
{
    public class MQHelperFactory
    {
        public static RabbitMQHelper Default(string changeName = "exchange_fanout") =>
            new RabbitMQHelper(changeName);
    }
}
