﻿using LinkingCloud.API.IRepository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LinkingCloud.API.Repository.Base
{
    public class BaseRepository<TEntity> : DbContext<TEntity>, IBaseRepository<TEntity> where TEntity : class, new()
    {
        /// <summary>
        /// 写入实体数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> Add(TEntity model)
        {
            var i = await Task.Run(() => Db.Insertable(model).ExecuteCommand());
            //返回的i是long类型,这里你可以根据你的业务需要进行处理
            return Convert.ToInt32(i) > 0 ? true : false;
        }

        /// <summary>
        /// 写入实体数据
        /// 使用于自增列
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> Insert(TEntity model)
        {
            var i = await Task.Run(() => Db.Insertable(model).ExecuteReturnIdentity());
            //返回的i是long类型,这里你可以根据你的业务需要进行处理
            return Convert.ToInt32(i) > 0 ? true : false;
        }

        public async Task<bool> DeleteByIds(object[] ids)
        {
            var i = await Task.Run(() => Db.Deleteable<TEntity>().In(ids).ExecuteCommand());
            throw new NotImplementedException();
        }

        public async Task<TEntity> QueryByID(object objId)
        {
            return await Task.Run(() => Db.Queryable<TEntity>().InSingle(objId));
        }

        public async Task<bool> Update(TEntity model)
        {
            var i = await Task.Run(() => Db.Updateable(model).ExecuteCommand());
            return Convert.ToInt32(i) > 0 ? true : false;
        }
    }
}
