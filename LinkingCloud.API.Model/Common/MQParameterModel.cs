﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkingCloud.API.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class MQAPIParameterModel
    {
        /// <summary>
        /// 接口域名
        /// </summary>
        public string webApi { get; set; }
        /// <summary>
        /// 接口Controller
        /// </summary>
        public string webapiModule { get; set; }
        /// <summary>
        /// 接口名
        /// </summary>
        public string operating { get; set; }
        /// <summary>
        /// 参数
        /// </summary>
        public string param { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        public string batchId { get; set; }
    }
}
